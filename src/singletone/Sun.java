package singletone;

public class Sun {
    private Integer temperatureInCelsius;

    static Sun sun = new Sun();

    public static Sun getSunInstance() {
        return sun;
    }

    public int getTemperatureInCelsius() {
        return temperatureInCelsius;
    }

    public void setTemperatureInCelsius(int temperatureInCelsius) {
        this.temperatureInCelsius = temperatureInCelsius;
    }

}
