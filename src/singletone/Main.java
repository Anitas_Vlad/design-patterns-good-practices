package singletone;

public class Main {
    public static void main(String[] args) {

        Sun sun1 = Sun.getSunInstance();
        sun1.setTemperatureInCelsius(1);
        System.out.println("Sun after the first getSunInstance call: " + sun1.getTemperatureInCelsius());

        Sun sun2 = Sun.getSunInstance();
        sun2.setTemperatureInCelsius(2);
        System.out.println("Sun after the second getSunInstance call: " + sun2.getTemperatureInCelsius());

        System.out.println("Sun1 : " + sun1.getTemperatureInCelsius());
        System.out.println("Sun1 : " + sun2.getTemperatureInCelsius());


    }
}
