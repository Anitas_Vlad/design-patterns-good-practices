package exercises.exercisesSDA.singletone;

import java.util.HashSet;
import java.util.stream.Collectors;

public class Servers {

    private HashSet<String> listOfServers = new HashSet<>();
    private static Servers instance = new Servers();

    public static Servers getSeversInstance() {
        return instance;
    }

    public boolean addServer(String server) {
        if (!listOfServers.contains(server)) {
            if (server.matches("^(http|https)")) {
                listOfServers.add(server);
                return true;
            }
        }
        return false;
    }

    public HashSet<String> downloadHTTP() {
        return listOfServers.stream().filter(each -> each.startsWith("http")).collect(Collectors.toCollection(HashSet<String>::new));
    }

    public HashSet<String> downloadHTTPS() {
        return listOfServers.stream().filter(each -> each.startsWith("https")).collect(Collectors.toCollection(HashSet<String>::new));
    }
}
