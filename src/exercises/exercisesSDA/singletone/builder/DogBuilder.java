package exercises.exercisesSDA.singletone.builder;

import java.util.List;
import java.util.Scanner;

public class DogBuilder {
    private String name;
    private String type;
    private Integer age;
    private List<String> toys;

    public DogBuilder hasName(String name) {
        this.name = name;
        return this;
    }

    public DogBuilder hasType(String type) {
        this.type = type;
        return this;
    }

    public DogBuilder hasAge(Integer age) {
        this.age = age;
        return this;
    }

    public DogBuilder hasToys(List<String> toys) {
        this.toys = toys;
        return this;
    }

    public Dog Build() {
        return new Dog(name, type, age, toys);
    }
}
