package exercises.myExercises.Factory;

import abstractFactory.Car;
import abstractFactory.Tesla.TeslaFactory;
import exercises.myExercises.Factory.Audi.AudiFactory;

import java.util.Optional;

public class Main {
    public static void main(String[] args) {
        AudiFactory audiFactory = new AudiFactory();
        TeslaFactory teslaFactory = new TeslaFactory();

        Optional<Car> car = audiFactory.createCar(140);
        if (car.isEmpty()) {
            System.out.println("Not enough Credit");
        } else {
            Car myCar = car.get();
            System.out.println(myCar.getCarModel());
        }
    }
}
