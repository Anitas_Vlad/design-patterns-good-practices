package exercises.myExercises.Factory.Tesla;

import exercises.myExercises.Factory.Car;

import java.util.Optional;

public class TeslaFactory {
    public Optional<Car> createCar(int budget){
        if (budget >= 150 && budget < 250){
            return Optional.of(new S("Tesla S"));
        }else if (budget >= 250){
            return  Optional.of(new CyberTruck("Cyber Truck"));
        }else return Optional.empty();
    }
}
