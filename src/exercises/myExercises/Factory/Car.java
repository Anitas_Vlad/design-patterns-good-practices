package exercises.myExercises.Factory;

public class Car {
    protected String model;

    public Car(String model){
        this.model = model;
    }

    public String getModel(){
        return model;
    }
}
