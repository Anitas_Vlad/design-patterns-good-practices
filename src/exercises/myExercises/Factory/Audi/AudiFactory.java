package exercises.myExercises.Factory.Audi;

import abstractFactory.Audi.A7;
import abstractFactory.Audi.Q8;
import abstractFactory.Car;

import java.util.Optional;

public class AudiFactory {
    public Optional<Car> createCar(int budget){
        if (budget >= 100 && budget <200){
            return Optional.of(new A7("Audi A7"));
        }else if (budget >= 200){
            return Optional.of(new Q8("Audi Q8"));
        }else return Optional.empty();
    }
}
