package exercises.myExercises.SingleTone;

public class Sun {
    private Integer temperatureInCelsius;

    static Sun sun = new Sun();

    static Sun getSunInstance(){
        return sun;
    }

    void setTemperatureInCelsius(Integer temperatureInCelsius){
        this.temperatureInCelsius = temperatureInCelsius;
    }

    Integer getTemperatureInCelsius(){
        return temperatureInCelsius;
    }
}
