package exercises.myExercises.SingleTone;

public class Main {
    public static void main(String[] args) {
        Sun sun1 = Sun.getSunInstance();
        sun1.setTemperatureInCelsius(111);
        System.out.println("Sun1 : " + sun1.getTemperatureInCelsius());

        Sun sun2 = Sun.getSunInstance();
        sun2.setTemperatureInCelsius(222);
        System.out.println("Sun2 : " + sun2.getTemperatureInCelsius());

        System.out.println("Sun1 : " + sun1.getTemperatureInCelsius());
        System.out.println("Sun2 : " + sun2.getTemperatureInCelsius());

    }
}
