package exercises.myExercises.ForAbstractFactory;

public class Car {

    protected String model;

    public Car(String model){
        this.model = model;
    }

    public String getModel() {
        return model;
    }
}
