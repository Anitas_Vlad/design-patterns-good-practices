package exercises.myExercises.ForAbstractFactory;

import exercises.myExercises.ForAbstractFactory.Audi.AudiFactory;
import exercises.myExercises.ForAbstractFactory.Tesla.TeslaFactory;

import java.util.Optional;

public class CarDealer {

    private final AudiFactory audiFactory = new AudiFactory();
    private final TeslaFactory teslaFactory = new TeslaFactory();

    public Optional<Car> orderCar(int budget, boolean isElectric) {
        if (isElectric) {
            return teslaFactory.createCar(budget);
        } else return audiFactory.createCar(budget);
    }
}
