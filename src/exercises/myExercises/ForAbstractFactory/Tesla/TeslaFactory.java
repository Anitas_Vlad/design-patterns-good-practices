package exercises.myExercises.ForAbstractFactory.Tesla;

import exercises.myExercises.ForAbstractFactory.Car;

import java.util.Optional;

public class TeslaFactory {
    public Optional<Car> createCar(int budget) {
        if (budget >= 150 && budget < 250) {
            return Optional.of(new CyberTruck("Cyber Truck"));
        } else if (budget >= 250) {
            return Optional.of(new S("Tesla S"));
        } else return Optional.empty();
    }
}
