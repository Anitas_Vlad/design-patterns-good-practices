package exercises.myExercises.ForAbstractFactory;

import java.util.Optional;

public class Main {
    public static void main(String[] args) {

        CarDealer carDealer = new CarDealer();

        Optional<Car> optionalCar = carDealer.orderCar(160, false);

        if (optionalCar.isEmpty()){
            System.out.println("You're too poor to buy a car");
        }else {
            Car car = optionalCar.get();
            System.out.println(car.getModel());
        }
    }
}
