package observer;

public class CustomObserver extends Observer{

    public CustomObserver(String name) {
        super(name);
    }

    @Override
    public void reactToSubjectStateChange() {
        System.out.println("I am " + name + " and it seems that the subject has a new value of:  " + subject.getState());
        System.out.println("I'll check it when I want");
    }
}
