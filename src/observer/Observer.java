package observer;

public class Observer {

    protected String name;
    protected Subject subject;

    public Observer(String name) {
        this.name = name;
    }

    public void observe(Subject subject) {
        this.subject = subject;
        subject.registerAsObserver(this);
    }

    public void reactToSubjectStateChange() {
        System.out.println("I am " + name + " and my subject's state is " + subject.getState());

        if (subject.getState() <= 3500) {
            System.out.println("Danger!");
        } else if (subject.getState() <= 3800) {
            System.out.println("Warning!");
        }else{
            System.out.println("No action!");
        }
    }

}
