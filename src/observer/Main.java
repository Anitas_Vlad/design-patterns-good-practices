package observer;

public class Main {
    public static void main(String[] args) {
        Subject oilSensor = new Subject(4000);
        Observer carComputer = new Observer("Car Computer (cluster)");
        Observer mechanicsComputerSystem = new CustomObserver("The mechanics' computer system");



        carComputer.observe(oilSensor);
        mechanicsComputerSystem.observe(oilSensor);

        oilSensor.changeState(100);

    }
}
