package observer;

import java.util.HashSet;
import java.util.Set;

public class Subject {
    protected int state;

    private Set<Observer> observers;

    public Subject(int state) {
        this.state = state;
        observers = new HashSet<>();
    }

    public void registerAsObserver(Observer observer) {
        observers.add(observer);
    }

    public int getState() {
        return state;
    }

    public void changeState(int amount) {
        state -= amount;
        for (Observer each : observers) {
            each.reactToSubjectStateChange();
        }
    }
}
