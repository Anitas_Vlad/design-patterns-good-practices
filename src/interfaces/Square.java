package interfaces;

public class Square implements Colorful{

    private String color;
    private double length;

    public double getArea(){
        return length*length;
    }

    public String getColor(){
        return color;
    }
}
