package interfaces;

public interface Colorful {

    String getColor();
}
