package GiveSomeMoney;

import java.util.Optional;

public class Person {

    private Wallet wallet;

    public void setWallet(Wallet wallet) {
        this.wallet = wallet;
    }

    public Optional<Integer> getMoney(int amount){
        return Optional.of(wallet.takeMoney(amount));
    }
}
