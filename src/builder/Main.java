package builder;

import builder.enums.PizzaDough;

public class Main {
    public static void main(String[] args) {

        Pizza pizza = new PizzaBuilder() // nu mai e pus intr-o variabila si e bun pentru garbageColector
                .withMediumDough()
                .withMeat()
                .withCheese()
                .build();
        System.out.println(pizza.toString());
    }
}
