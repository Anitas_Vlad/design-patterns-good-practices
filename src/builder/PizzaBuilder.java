package builder;

import builder.enums.PizzaDough;

public class PizzaBuilder {
    private PizzaDough pizzaDough;
    private boolean hasCheese;
    private boolean hasSauce;
    private boolean hasMeat;

    public PizzaBuilder withCheese() {
        hasCheese = true;
        return this;
    }

    public PizzaBuilder withSauce() {
        hasSauce = true;
        return this;
    }

    public PizzaBuilder withMeat() {
        hasMeat = true;
        return this;
    }

    public PizzaBuilder withMediumDough() {
        pizzaDough = PizzaDough.MEDIUM;
        return this;
    }

    public PizzaBuilder withMLargeDough() {
        pizzaDough = PizzaDough.MEDIUM;
        return this;
    }

    public Pizza build() {
        Pizza pizza = new Pizza();
        pizza.setPizzaDough(pizzaDough);
        pizza.setHasCheese(hasCheese);
        pizza.setHasSauce(hasSauce);
        pizza.setHasMeat(hasMeat);
        return pizza;
    }
}
