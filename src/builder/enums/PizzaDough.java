package builder.enums;

public enum PizzaDough {
    THIN, MEDIUM, LARGE;
}
