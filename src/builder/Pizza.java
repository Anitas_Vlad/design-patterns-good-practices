package builder;

import builder.enums.PizzaDough;

public class Pizza {
    private PizzaDough pizzaDough;
    private boolean hasCheese;
    private boolean hasSauce;
    private boolean hasMeat;

    public PizzaDough getPizzaDough() {
        return pizzaDough;
    }

    public void setPizzaDough(PizzaDough pizzaDough) {
        this.pizzaDough = pizzaDough;
    }

    public boolean isHasCheese() {
        return hasCheese;
    }

    public void setHasCheese(boolean hasCheese) {
        this.hasCheese = hasCheese;
    }

    public boolean isHasSauce() {
        return hasSauce;
    }

    public void setHasSauce(boolean hasSauce) {
        this.hasSauce = hasSauce;
    }

    public boolean isHasMeat() {
        return hasMeat;
    }

    public void setHasMeat(boolean hasMeat) {
        this.hasMeat = hasMeat;
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "pizzaDough=" + pizzaDough +
                ", hasCheese=" + hasCheese +
                ", hasSauce=" + hasSauce +
                ", hasMeat=" + hasMeat +
                '}';
    }
}
