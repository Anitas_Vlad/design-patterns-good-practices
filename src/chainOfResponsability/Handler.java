package chainOfResponsability;

import java.util.Random;

public class Handler {
    private String name;
    private Handler nextHandler;

    public Handler(String name) {
        this.name = name;
    }

    public boolean giveMeAPen() {
        Random randomBoolean = new Random();
        boolean hasPan = randomBoolean.nextBoolean();
        if (hasPan) {
            System.out.println("I am " + name + " and will offer you a pen");
            return true;
        }
        if (nextHandler == null) {
            System.out.println("I have no pen, neither a teammate.");
            return false;
        }
        System.out.println("I'm penless " + name + " and I'll call my teammate to give you a pen");
        return nextHandler.giveMeAPen();
    }

    public void setNextHandler(Handler nextHandler) {
        this.nextHandler = nextHandler;
    }
}
