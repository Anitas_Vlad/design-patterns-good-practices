package chainOfResponsability;

public class Main {
    public static void main(String[] args) {

        Handler alexandru = new Handler("Alexandru");
        Handler monica = new Handler("Monica");
        Handler daniela = new Handler("Daniela");
        Handler alexandra = new Handler("Alexandra");

        alexandru.setNextHandler(monica);
        monica.setNextHandler(daniela);
        daniela.setNextHandler(alexandra);

        alexandru.giveMeAPen();
    }
}
