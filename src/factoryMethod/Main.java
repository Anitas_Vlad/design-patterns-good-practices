package factoryMethod;

import java.util.Optional;

public class Main {
    public static void main(String[] args) {
        TeslaFactory teslaFactory = new TeslaFactory();
        Optional<Car> optionalCar = teslaFactory.createCar(90000);
        if (optionalCar.isEmpty()){
            System.out.println("You're too poor for a Tesla Car");
        }else{
            Car car = optionalCar.get();
            System.out.println(car.getCarModel());
        }
    }
}
