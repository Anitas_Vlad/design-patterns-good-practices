package factoryMethod;

public class Car {

    protected String carModel;

    public Car(String carModel) {
        this.carModel = carModel;
    }
    public String getCarModel(){
        return carModel;
    }
}
