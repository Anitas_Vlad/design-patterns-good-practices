package abstractFactory;

import abstractFactory.Tesla.TeslaFactory;

import java.util.Optional;

public class Main {
    public static void main(String[] args) {

        CarDealer carDealer = new CarDealer();
        Optional<Car> optionalCar = carDealer.orderCar(90000, true);

        if (optionalCar.isEmpty()){
            System.out.println("You're too poor for a Tesla Car");
        }else{
            Car car = optionalCar.get();
            System.out.println(car.getCarModel());
        }
    }
}
