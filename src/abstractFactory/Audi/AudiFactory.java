package abstractFactory.Audi;

import abstractFactory.Car;

import java.util.Optional;

public class AudiFactory {

    public Optional<Car> createCar(int budget){
        if (budget >=50000 && budget <100000){
            return Optional.of(new A7("Audi A7"));
        }
        if (budget >= 100000){
            return Optional.of(new Q8("Audi Q8"));
        }
        return Optional.empty();
    }
}
