package abstractFactory;

import abstractFactory.Audi.AudiFactory;
import abstractFactory.Tesla.TeslaFactory;


import java.util.Optional;

public class CarDealer {

    private AudiFactory audiFactory = new AudiFactory();
    private TeslaFactory teslaFactory = new TeslaFactory();

    public Optional<Car> orderCar(int budget, boolean isElectric){
        if (isElectric){
            return teslaFactory.createCar(budget);
        }else{
            return audiFactory.createCar(budget);
        }
    }
}
