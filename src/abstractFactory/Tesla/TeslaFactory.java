package abstractFactory.Tesla;

import abstractFactory.Car;

import java.util.Optional;

public class TeslaFactory {

    public Optional<Car> createCar(int budget) {
        if (budget >= 70000 && budget < 90000) {
            return Optional.of(new TeslaS("Tesla S"));
        }
        if (budget >= 90000 && budget < 120000) {
            return Optional.of(new TeslaX("Tesla X"));
        }

        if (budget >= 120000) {
            return Optional.of(new TeslaS("Cyber Truck"));
        }

        return Optional.empty();
    }
}
